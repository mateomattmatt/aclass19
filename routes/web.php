<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Topic;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::get('create','ImageController@create');
Route::post('create','ImageController@store');
Route::get('api/v2/topics/{id}', 'TopicAPIController@show');
Route::post('/join/class', 'JoinController@join');

Route::group(['prefix' => 'topic', 'middleware' => ['role:member']], function () {
    Route::get('/{id}', 'HomeController@view');
    Route::post('/join', 'HomeController@join');
    Route::post('/unjoin', 'HomeController@unjoin');
});

Route::group(['prefix' => 'topics', 'middleware' => ['role:admin|owner']], function () {
    Route::post('/find', 'HomeController@findStudent');
    Route::get('/', 'TopicController@index');
    Route::get('/add', 'TopicController@create');
    Route::post('/add', 'TopicController@store');
    Route::get('/{id}', 'TopicController@show');
    Route::get('/{id}/users', 'TopicController@users');
    Route::get('/{id}/edit', 'TopicController@edit');
    Route::post('/{id}/edit', 'TopicController@update');
    Route::post('/users', 'TopicController@test');
    Route::delete('/{id}/delete', 'TopicController@destroy');
});

Route::group(['prefix' => 'user'],function() {
    Route::post('/{id}', 'UserController@update');
    Route::post('/{id}/changepassword', 'UserController@updatepassword');
});

Route::get('/profile/changepassword', 'HomeController@changepassword');

Route::group(['prefix' => 'users', 'middleware' => ['role:admin|owner']], function () {
    Route::get('/', 'UserController@index');
    Route::post('/', 'UserController@remove');
    Route::get('/add', 'UserController@create');
    Route::post('/add', 'UserController@store');
    Route::get('/{id}', 'UserController@show');
    Route::post('/{id}', 'UserController@update');
    Route::get('/{id}/changepassword', 'UserController@changepassword');
    Route::post('/{id}/changepassword', 'UserController@updatepassword');
    Route::delete('/', 'UserController@destroy');
});

Route::group(['prefix' => 'logs', 'middleware' => ['role:owner']], function () {
    Route::get('/', 'LogController@index');
});

Route::group(['prefix' => 'api/v2', 'middleware' => ['role:admin|owner']], function () {
    Route::get('/topics', 'TopicAPIController@index');
    Route::post('/topics', 'TopicAPIController@create');
    Route::post('/topics/{id}', 'TopicAPIController@update');
    Route::delete('/topics/{id}', 'TopicAPIController@destroy');
});

Route::get('/terms', function() {
    return view('terms');
});

