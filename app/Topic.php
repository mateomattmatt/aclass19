<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Topic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'topics';
    protected $fillable = [
        'title', 'description', 'image_id', 'capacity, speaker, session, venue'
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_topics', 'topic_id', 'user_id');
    }

    public function image()
    {
        return $this->belongsTo('App\Image', 'image_id', 'id');
    }
}
