<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'message', 'ip_address', 'user_agent',
        'city', 'region', 'country', 'loc', 'postal', 'org'
    ];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
