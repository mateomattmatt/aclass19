<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Image;
use App\Topic;
use App\Log;
use DB;
use Auth;
use ImageIntervention;
use Illuminate\Support\Facades\Validator;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \DataTables::model(new Log)->searchable('id', 'message', 'user.email', 'user.first_name', 'user.last_name', 'user.student_id', 'ip_address', 'user_agent', 'city', 'region', 'country', 'loc', 'postal', 'org')->with('user')->get();
        return view('modules.logs.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return back()->with('success', 'Your image has been successfully saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topics = Topic::find($id);
        return view('topics', ['topic' => $topics]);
    }
}
