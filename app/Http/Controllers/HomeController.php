<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Log;
use App\User;
use App\Topic;
use App\UserTopic;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $topics = Topic::with(['users', 'image'])->get();
        $user = User::where('id', Auth::user()->id)->with('topics')->first();
        return view('home', ['topics' => $topics, 'user' => $user]);
    }

    public function profile() {
        $user = User::find(Auth::user()->id);
        return view('modules.home.profile', ['user' => $user]);
    }

    public function changepassword() {
        $user = User::find(Auth::user()->id);
        return view('modules.home.changepassword', ['user' => $user]);
    }

    public function view($id) {
        $topic = Topic::where('id', $id)->with(['users', 'image'])->first();
        $isJoin = UserTopic::where('user_id', Auth::user()->id)->get();
        $isTopic = UserTopic::where('user_id', Auth::user()->id)->where('topic_id', $id)->get();

        if (count($isJoin) > 0) {
            $isJoin = 1;
        } else {
            $isJoin = 0;
        }
        if (count($isTopic) > 0) {
            $isTopic = 1;
        } else {
            $isTopic = 0;
        }
        return view('modules.home.topic', ['topic' => $topic, 'isJoin' => $isJoin, 'isTopic' => $isTopic]);
    }

    public function join(Request $request) {
        $topic = Topic::find($request->id);
        $user = Auth::user();

        if (!empty($topic)) {
            $user_topic = UserTopic::where('user_id', $user->id)->get();
            if (count($user_topic) == 0) {
                $_topic = Topic::where('id', $request->id)->with('users')->first();
                
                if (count($_topic->users) < $_topic->capacity) {
                    $ut = new UserTopic();
                    $ut->user_id = $user->id;
                    $ut->topic_id = $topic->id;
                    $ut->save();

                    \AppHelper::instance()->log_activity(Auth::user()->id, 'Registered to topic: '.$topic->title);
                    return [
                        'status' => 'success',
                        'title' => 'Success',
                        'message' => 'Successfully join the class',
                    ];
                } else {
                    return [
                        'status' => 'error',
                        'title' => 'Class is already full',
                        'message' => 'Unable to join this class'
                    ];
                }
            } else {
                return [
                    'status' => 'error',
                    'title' => 'Error',
                    'message' => 'You already join a class.'
                ];
            }
        } else {
            return [
                'status' => 'warning',
                'title' => 'Warning',
                'message' => 'Topic not found. Contact for support.'
            ];
        }
    }

    public function unjoin(Request $request) {
        $topic = Topic::find($request->id);
        $user = Auth::user();

        if (!empty($topic)) {
            $_topic = UserTopic::where('user_id', Auth::user()->id)->where('topic_id', $topic->id);
            $_topic->delete();

            \AppHelper::instance()->log_activity(Auth::user()->id, 'Unregistered to topic: '.$topic->title);
            return [
                'status' => 'success',
                'title' => 'Success',
                'message' => 'Successfully removed yourself from this class.'
            ];
        } else {
            return [
                'status' => 'warning',
                'title' => 'Warning',
                'message' => 'Topic not found. Contact for support.'
            ];
        }
    }

    public function findStudent(Request $request) {
        $user = User::where('student_id', $request->id)->with('topics')->first();
        return $user;
    }
}
