<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Image;
use App\Topic;
use DB;
use ImageIntervention;
use Illuminate\Support\Facades\Validator;

class TopicAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DB::table('topics')->join('images', 'topics.image_id', '=', 'images.id')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'filename' => 'image|required|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $original_image = $request->file('filename');
        $thumbnail_image = ImageIntervention::make($original_image);
        $thumbnail_path = public_path().'/images/thumbnail//';
        $original_path = public_path().'/images/original//';
        $thumbnail_image->save($original_path.time().$original_image->getClientOriginalName());
        $thumbnail_image->resize(150, 150);
        $thumbnail_image->save($thumbnail_path.time().$original_image->getClientOriginalName());

        $image = new Image();
        $image->name = time();
        $image->filename = time().$original_image->getClientOriginalName();
        $image->save();

        $topic = new Topic();
        $topic->image_id = $image->id;
        $topic->title = $request->title;
        $topic->description = $request->description;
        $topic->capacity = $request->capacity;
        $topic->save();

        // return $request;
        return redirect('/home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        return back()->with('success', 'Your image has been successfully saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Topic::where('topics.id', $id)->leftJoin('images', 'topics.image_id', '=', 'images.id')->select('topics.id', 'topics.title', 'topics.description', 'topics.capacity', 'images.filename')->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $topic = Topic::findOrFail($id);
        $topic->update($request->all());
        return redirect('/topics/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Topic::find($id)->delete();
        return 204;
    }
}
