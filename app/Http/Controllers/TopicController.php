<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Image;
use App\Topic;
use App\UserTopic;
use App\User;
use DB;
use Auth;
use ImageIntervention;
use Illuminate\Support\Facades\Validator;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \DataTables::model(new Topic)->searchable('id', 'title', 'description', 'capacity')->with(['image', 'users'])->get();
        return view('modules.topics.index');
    }

    public function users($id) {
        $users = User::all();
        $topic = Topic::find($id);
        return view('modules.topics.users', ['topic' => $topic, 'users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('modules.topics.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $topic = new Topic();
        $image = new Image();

        if (!empty($request->file('filename'))) {
            $this->validate($request, [
                'filename' => 'image|required|mimes:jpeg,png,jpg,gif,svg'
            ]);
            $time = time();
            $original_image = $request->file('filename');
            $thumbnail_image = ImageIntervention::make($original_image);
            $thumbnail_path = public_path().'/images/thumbnail//';
            $original_path = public_path().'/images/original//';
            $thumbnail_image->save($original_path.$time.$original_image->getClientOriginalName());
            $thumbnail_image->resize(150, 150);
            $thumbnail_image->save($thumbnail_path.$time.$original_image->getClientOriginalName());

            $image->name = $time;
            $image->filename = $time.$original_image->getClientOriginalName();
            $image->save();
            $topic->image_id = $image->id;
        }
        $topic->title = $request->title;
        $topic->description = $request->description;
        $topic->capacity = $request->capacity;
        $topic->speaker = $request->speaker;
        $topic->venue = $request->venue;
        $topic->session = $request->session;
        $topic->save();

        \AppHelper::instance()->log_activity(Auth::user()->id, 'Added topic: '.$topic->title);
        return back()->with('success', 'Your topic has been successfully saved.');
    }
    function test(Request $request) {
        $users = $request->user;
        for ($i=0; $i < count($users); $i++) { 
            $id = $users[$i];
            $topic = Topic::where('id', $request->topic)->with('users')->first();
            
            if (count($topic->users) < $topic->capacity) {
                $ut = new UserTopic();
                $ut->user_id = $id;
                $ut->topic_id = $request->topic;
                $ut->save();
            }
        }
        return back()->with('success', 'Users is added');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topics = Topic::find($id);
        return view('modules.topics.view', ['topic' => $topics]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topics = Topic::where('id',$id)->with('image')->first();
        return view('modules.topics.edit', ['topic' => $topics]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'filename' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        $topic = Topic::find($id);
        if (!empty($request->filename)) {
            $time = time();
            $original_image = $request->file('filename');
            $thumbnail_image = ImageIntervention::make($original_image);
            $thumbnail_path = public_path().'/images/thumbnail//';
            $original_path = public_path().'/images/original//';
            $thumbnail_image->save($original_path.$time.$original_image->getClientOriginalName());
            $thumbnail_image->resize(150, 150);
            $thumbnail_image->save($thumbnail_path.$time.$original_image->getClientOriginalName());

            $image = new Image();
            $image->name = $time;
            $image->filename = $time.$original_image->getClientOriginalName();
            $image->save();
            $topic->image_id = $image->id;
        }
        $topic->title = $request->title;
        $topic->description = $request->description;
        $topic->capacity = $request->capacity;
        $topic->speaker = $request->speaker;
        $topic->venue = $request->venue;
        $topic->session = $request->session;
        $topic->save();
        \AppHelper::instance()->log_activity(Auth::user()->id, 'Updated topic: '.$topic->title);

        // return $request;
        return back()->with('success', 'Your topic has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = UserTopic::where('topic_id', '=', $id);
        $users->delete();
        $topic = Topic::find($id);
        \AppHelper::instance()->log_activity(Auth::user()->id, 'Deleted topic: '.$topic->title);
        $topic->delete();
    }
}
