<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Topic;
use App\UserTopic;
use App\User;
use Auth;
use ImageIntervention;
use Illuminate\Support\Facades\Validator;

class JoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function join(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user_topic = UserTopic::where('user_id', '=', Auth::user()->id)->with(['user', 'topic'])->get();
        if (count($user_topic) > 0) {
            $user_topic[0]->delete();
        }
        $user_topic = new UserTopic();
        $user_topic->user_id = Auth::user()->id;
        $user_topic->topic_id = $request->id;
        $user_topic = UserTopic::where('user_id', '=', Auth::user()->id)->with(['user', 'topic'])->get();
        return $user_topic;
    }
}
