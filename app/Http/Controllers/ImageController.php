<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Image;
use ImageIntervention;
use Illuminate\Support\Facades\Validator;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('createimage');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'filename' => 'image|required|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $original_image = $request->file('filename');
        $thumbnail_image = ImageIntervention::make($original_image);
        $thumbnail_path = public_path().'/thumbnail';
        $original_path = public_path().'/images';
        $thumbnail_image->save($original_path.time().$original_image->getClientOriginalName());
        $thumbnail_image->resize(150, 150);
        $thumbnail_image->save($thumbnail_path.time().$original_image->getClientOriginalName());

        $image = new Image();
        $image->name = time();
        $image->filename = time().$original_image->getClientOriginalName();
        $image->save();

        return back()->with('success', 'Your image has been successfully saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
