<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Image;
use App\Topic;
use App\User;
use App\UserTopic;
use App\Role;
use DB;
use Auth;
use ImageIntervention;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            \DataTables::model(new User)->searchable('id', 'first_name', 'last_name', 'email', 'student_id', 'roles.name')->with('roles')->get();
            return view('modules.users.index');
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('modules.users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'student_id' => ['required', 'string', 'min:8', 'max:8', 'regex:/\d{2}-\d{5}/', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        $user = new User();
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->student_id = $request['student_id'];
        $user->email = $request['email'];
        $user->password = Hash::make($request['password']);
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->save();

        $role = Role::where('name', '=', $request['role'])->first();
        $user->attachRole($role);

        \AppHelper::instance()->log_activity(Auth::user()->id, 'Created new Account: '.$user->first_name.' '.$user->last_name);
        return back()->with('success', 'Successfully added new user.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('modules.users.view', ['user' => $user]);

    }

    public function remove(Request $request) {
        if ($request->id) {
            $_topic = UserTopic::where('user_id', $request->id);
            $_topic->delete();
            return [
                'status' => 'success'
            ];
        } else {
            return [
                'status' => 'error'
            ];
        }
    }

    public function changepassword($id)
    {
        $user = User::find($id);
        return view('modules.users.changepassword', ['user' => $user]);
    }

    public function updatepassword(Request $request, $id)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        $user = User::find($id);
        $user->password = Hash::make($request['password']);
        $user->save();

        \AppHelper::instance()->log_activity(Auth::user()->id, 'Change password of '.$user->first_name.' '.$user->last_name. ' to {'.$request->password.'}');
        return back()->with('success', 'Successfully updated password.');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'student_id' => ['required', 'string', 'min:8', 'max:8', 'regex:/\d{2}-\d{5}/', 'unique:users,student_id,'.$id],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
        ]);

        $user = User::find($id);
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->student_id = $request['student_id'];
        $user->email = $request['email'];
        $user->save();

        //$role = Role::where('name', '=', $request['role'])->first();
       // $user->detachRoles($user->roles);
       // $user->attachRole($role);

        \AppHelper::instance()->log_activity(Auth::user()->id, 'Updated account of '.$user->first_name.' '.$user->last_name);
        return back()->with('success', 'Successfully updated user.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $user_topic = UserTopic::where('user_id', '=', $id);
        $user_topic->delete();
        $user = User::find($id);
        \AppHelper::instance()->log_activity(Auth::user()->id, 'Deleted account of '.$user->first_name.' '.$user->last_name);
        $user->detachRoles($user->roles);
        $user->delete();
        return [
            'status' => 'success',
            'title' => 'Success',
            'message' => 'Successfully deleted user',
        ];
    }
}
