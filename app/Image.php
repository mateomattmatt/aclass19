<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'images';
    protected $fillable = [
        'name', 'filename'
    ];

    public function topic() {
        return $this->belongsTo('App\Topic');
    }
}
