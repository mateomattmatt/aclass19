<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner = new Role();
        $owner->name         = 'owner';
        $owner->display_name = 'Project Owner'; // optional
        $owner->description  = 'Maintainer'; // optional
        $owner->save();

        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'User Administrator'; // optional
        $admin->description  = 'User is allowed to manage and edit other users'; // optional
        $admin->save();

        $member = new Role();
        $member->name         = 'member';
        $member->display_name = 'Member'; // optional
        $member->description  = 'Register to subjects created by admin'; // optional
        $member->save();

        $admin_access = new Permission();
        $admin_access->name         = 'admin-access';
        $admin_access->display_name = 'Admin Access'; // optional
        $admin_access->description  = 'Full control on the administrator settings.'; // optional
        $admin_access->save();

        $member_access = new Permission();
        $member_access->name         = 'member-access';
        $member_access->display_name = 'Member Access'; // optional
        $member_access->description  = ''; // optional
        $member_access->save();

        $admin->attachPermission($admin_access);
        $owner->attachPermissions(array($admin_access, $member_access));
        $member->attachPermission($member_access);

        $user_owner = new User();
        $user_owner->first_name      = 'Benedict';
        $user_owner->last_name       = 'Mateo';
        $user_owner->student_id      = '14-00247';
        $user_owner->email           = 'mattmateo100@gmail.com';
        $user_owner->password        = Hash::make('12345');
        $user_owner->email_verified_at = '2019-01-01 00:00:00';
        $user_owner->save();

        $user_owner->attachRole($owner);
    }
}
