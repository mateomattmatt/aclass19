@extends('layouts.app')

@section('content')
<section>
    <div class="row float-card">
        <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
            <div class="card">
                <div class="card--header">
                    <h2>Welcome Back!</h2>
                    <p>Sign in using your email.</p>
                </div>
                <div class="card--main">
                    <form action="{{ route('login') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <div class="form-group text-left justify-content-start d-flex">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="remember">
                                    {{ __('Keep me signed in') }}
                                </label>
                            </div>
                            {{-- <a href="{{ route('password.request') }}">Forgot your password?</a> --}}

                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="btn btn--main">
                                Sign in
                            </button>
                        </div>
                    </form>
                </div>
                <div class="card--footer">
                    <div class="form-group text-center">
                        <a href="/terms">Terms and Conditions</a>
                    </div>
                    <div class="form-group text-center">
                        Don't have any account yet? <a href="{{route('register')}}">Join now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
