@extends('layouts.app')

@section('content')
    <div class="row float-card">
        <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
            <div class="card">
                <div class="card--header">
                    <h2>Email Verification</h2>
                    <p>Before proceeding, please check your email for a verification link.</p>
                </div>
                <div class="card--main">
                    @if (session('resent'))
                        <div class="alert alert-success mb-3" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                    <p class="text-center">{{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
