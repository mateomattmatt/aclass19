@extends('layouts.app')

@section('content')
    <div class="row float-card">
        <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
            <div class="card">
                <div class="card--header">
                    <h2>Forgot Password!</h2>
                    <p>Reset password link will be sent in your email.</p>
                </div>
                <div class="card--main">
                    @if (session('status'))
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3">
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        </div>
                    @endif
                    <form action="{{ route('password.email') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus placeholder="Email Address">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn--main btn-block">
                                {{ __('Reset Password') }}
                            </button>
                        </div>
                    </form>
                </div>
                <div class="card--footer">
                    <div class="form-group text-center">
                        Don't have any account yet? <a href="{{route('register')}}">Join now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
