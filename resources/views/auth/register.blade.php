@extends('layouts.app')

@section('content')
    <div class="row float-card">
        <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
            <div class="card">
                <div class="card--header">
                    <h2>Create an Account</h2>
                    <p>Enter your details and let's get started.</p>
                </div>
                <div class="card--main">
                    <form action="{{ route('register') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-12 col-md-6">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus placeholder="First Name">
                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus placeholder="Last Name">
                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">{{ $errors->first('last_name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <input id="student_id" type="text" class="form-control{{ $errors->has('student_id') ? ' is-invalid' : '' }}" name="student_id" value="{{ old('student_id') }}" required autofocus placeholder="Student ID">
                            @if ($errors->has('student_id'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('student_id') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email address">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                        </div>

                        <div class="form-group text-right">
                            <button type="submit" class="btn btn--main">
                                Sign up
                            </button>
                        </div>
                    </form>
                </div>
                <div class="card--footer">
                    <div class="form-group text-center">
                        <a href="/terms">Terms and Conditions</a>
                    </div>
                    <div class="form-group text-center">
                        Already have ACLASS account? <a href="{{route('login')}}">Sign in</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
