<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>UNC ACLASS</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link href="{{ asset('css/global.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <link href="{{ asset('css/comic.css') }}" rel="stylesheet">

        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/custom.js') }}" defer></script>
        <link rel="shortcut icon" href="{{asset('images/logo-new.png')}}">
    </head>
    <body>
        <div class="welcome">
            <div class="welcome-icon">
                <img class="img" src="{{ asset('images/aclass.png') }}" alt="">
            </div>
            <div class="row">
                <div class="col col--left col-6">
                    <div class="welcome-container">
                        <div class="text-container">

                            <h1 class="text-container--1">ACLASS 3.0</h1>
                            <h1 class="text-container--2">ACLASS 3.0</h1>
                            <h1 class="text-container--3">ACLASS 3.0</h1>
                            <h1 class="text-container--4">ACLASS 3.0</h1>
                            <h1 class="text-container--5">ACLASS 3.0</h1>
                            <h1 class="text-container--6">ACLASS 3.0</h1>
                        </div>
                    </div>
                </div>
                <div class="col col--right col-6">
                    <div class="welcome-container">
                        <div class="login">
                            <a href="{{ route('register') }}">register</a>
                            <a href="{{ route('login') }}">login</a>
                        </div>
                        <div class="slogan">
                            <p>Andare:</p>
                            <p> Championing Excellence Through Meaningful Actions and Applying one's self</p>
                        </div>
                        <div class="footer">
                            Copyright &copy; 2020
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
