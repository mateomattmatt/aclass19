
<section>
    <div class="row float-card">
        <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
            <div class="card">
                <div class="card--header">
                    <h2>Search Student</h2>
                    <p>Use student id to search for student</p>
                </div>
                <div class="card--main">
                    <form action="" id="student-id">
                        <div class="form-group">
                            <input type="text" name="student_id" class="form-control -large" placeholder="XX-XXXXX" required />
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn--main">Search</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')
    <script>
        $('#student-id').on('submit', function (e) {
            e.preventDefault();
            var student_id = $(this).serializeArray()[0].value;
            $.ajax({
                url: '/topics/find',
                data: { id: student_id },
                type: 'POST'
            }).done(function (result) {
                const { first_name, last_name, topics } = result;
                swal({
                    text: `
                    Name: ${first_name} ${last_name}
                    Topic: ${topics.length > 0 ? topics.map(function (topic) {
                        return topic.title;
                    }).join('\n') : '--'}
                    `,
                    title: 'Student info',
                    type: 'info'
                })
            }).fail(function (error) {
                swal('Error', 'Server Error Response, Contact Support', 'error');
            })
        })
    </script>
@endsection
