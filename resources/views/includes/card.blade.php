<section class="">
    <div class="container">
        <br>
        <div class="card">
            @if (count($user->topics) > 0)
                <h1><a href="/topic/{{$user->topics[0]->id}}">{{$user->topics[0]->title}}</a></h1>
                <p>You are currently registered to this topic.</p>
            @else
                <h1>Choose your Topic</h1>
                <p>Pick a class you want to attend and you can only choose one. Enjoy!</p>
            @endif
        </div>
        <br>
        <div class="row">
            @if (count($topics) > 0)
                @for($i = count($topics) - 1; $i >= 0; $i--)
                    @if ( $topics[$i]->capacity - count($topics[$i]->users) > 0)
                        <div class="col-12 col-sm-6 col-lg-4 mb-4">
                            <div class="card" style="min-height: 300px;">
                                <a href="/topic/{{$topics[$i]->id}}?_c={{md5(uniqid($topics[$i]->id + rand(), true))}},{{rand() * $topics[$i]->id}}&_t={{$topics[$i]->title}}&meta[]={{rand()}}" style="text-decoration: none;">
                                    @if ($topics[$i]->image)
                                        <div>
                                            <img src="{{asset('aclass19/public/images/original/'.$topics[$i]->image->filename)}}" alt="{{$topics[$i]->title}}" class="img-fluid">
                                        </div>
                                    @endif
                                    <div class="py-2">
                                        <h5>{{$topics[$i]->title}}</h5>
                                    </div>
                                    <p class="mb-0">Venue: {{ $topics[$i]->venue }}</p>
                                    <p class="mb-0">Schedule: {{ $topics[$i]->session }}</p>
                                    <p class="mb-0">Slots Left: {{ $topics[$i]->capacity - count($topics[$i]->users)}}</p> {{-- need to compute users --}}
                                    <div class="text-right" style="position: absolute; right: 20px; bottom: 20px;">
                                        <a href="/topic/{{$topics[$i]->id}}?_c={{md5(uniqid($topics[$i]->id + rand(), true))}},{{rand() * $topics[$i]->id}}&_t={{$topics[$i]->title}}&meta[]={{rand()}}">See more</a>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endif
                @endfor
            @else
                <div class="col-12">
                    <div class="card">
                        <h5 class="text-secondary">Topics are not available right now</h5>
                    </div>
                </div>
            @endif
        </div>
    </div>
</section>
