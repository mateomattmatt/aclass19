@extends('layouts.app')


@section('content')

@if(Auth::user()->hasRole('member'))
    @include('./includes/card')
@elseif(Auth::user()->hasRole(['owner','admin']))
    @include('./includes/admin')
@endif
@endsection
