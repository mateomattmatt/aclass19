@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <nav aria-label="breadcrumb bg-white">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="/topics">Topics</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$topic->title}}</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-12 col-md-8">
                    <div class="card mb-4">
                        <h3 class="mb-0">{{$topic->title}}</h3>
                    </div>
                    <div class="card mb-4">
                        <p>{{$topic->description}}</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    @if ($topic->image)
                        <div class="card mb-4">
                            <img src="{{asset('aclass19/public/images/original/'.$topic->image->filename)}}" alt="" class="img-fluid" />
                        </div>
                    @endif
                    <div class="card mb-4">
                        <p><i class="fas fa-map-marker"></i> {{$topic->venue}}</p>
                        <p><i class="fas fa-clock"></i> {{$topic->session}}</p>
                        <p><i class="fas fa-user"></i> {{$topic->speaker}}</p>
                        <p><i class="fas fa-bookmark"></i> {{$topic->capacity - count($topic->users)}} slots left</p>
                        <a href='/topics/{{$topic->id}}/edit'>Edit [{{$topic->title}}]</a>

                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="d-flex justify-content-between mb-3">
                            <h5>Currently Registered</h5>
                            <a class="btn btn-primary" href="/topics/{{$topic->id}}/users">Add More Student</a>
                        </div>
                        @if (count($topic->users) > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Student ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($topic->users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->student_id}}</td>
                                        <td>{{$user->first_name}}</td>
                                        <td>{{$user->last_name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td><button class="btn btn-danger btn--remove" data-id="{{$user->id}}"><i class="fa fa-trash text-white"></i></button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-danger">
                            <span>There are no users currently registered.</span>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascript')
    <script>
        $(document).on('click', '.btn--remove', function () {
            var id = $(this).data('id');
            swal({
                icon: 'warning',
                text: 'Are you sure you want to remove this user?',
                buttons: true,
                title: 'Remove User'
            }).then(function (result) {
                if (result) {
                    $.ajax({
                        type: 'POST',
                        url: '/users',
                        data: {
                            id: id
                        }
                    }).done(function (response) {
                        swal('Success', 'successfully removed user', 'success');
                        window.location.reload();
                    }).fail(function (error) {
                        swal('Error', 'Server error 400', 'error');
                    })
                }
            }).catch(function (error) {
                swal('Error', 'Something happened', 'error');
            })
        });
    </script>
@endsection
