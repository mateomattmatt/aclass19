@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <nav aria-label="breadcrumb bg-white">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="/topics">Topics</a></li>
                    <li class="breadcrumb-item"><a href="/topics/{{$topic->id}}">{{$topic->title}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Topic</li>
                </ol>
            </nav>
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="card">
                <h5>Edit Topic</h5>
                <div class="row">
                    <div class="col-12">
                        <form action="/topics/{{$topic->id}}/edit" method="POST" enctype="multipart/form-data" class="container-block__form text-left">
                            @csrf
                            <div class="form-group">
                                <label for="">Title</label>
                                <input type="text" class="form-control" name="title" value="{{$topic->title}}" required/>
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <textarea name="description" class="form-control" required>{{$topic->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-md-4 col-lg-3 text-center">
                                        @if($topic->image)
                                            <img src="{{asset('aclass19/public/images/original/'.$topic->image->filename)}}" alt="" class="img-fluid">
                                        @endif
                                    </div>
                                    <div class="col-12 col-md-8 col-lg-9">
                                        <label for="">Image</label>
                                        <input type="file" name="filename" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Capacity</label>
                                <input type="number" class="form-control" name="capacity" required value="{{$topic->capacity}}"/>
                            </div>
                            <div class="form-group">
                                <label for="">Speaker</label>
                                <input type="text" class="form-control" name="speaker" required value="{{$topic->speaker}}"/>
                            </div>
                            <div class="form-group">
                                <label for="">Session</label>
                                <input type="text" class="form-control" name="session" required value="{{$topic->session}}"/>
                            </div>
                            <div class="form-group">
                                <label for="">Venue</label>
                                <input type="text" class="form-control" name="venue" required value="{{$topic->venue}}"/>
                            </div>
                            <button class="btn btn-primary btn-block" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
