@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb bg-white">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/home">Home</a></li>
                <li class="breadcrumb-item"><a href="/topics">Topics</a></li>
                <li class="breadcrumb-item active" aria-current="page">Add Topic</li>
            </ol>
        </nav>
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <div class="card">
            <h5>Add Topic</h5>
            <div class="row">
                <div class="col-12">
                    <form action="/topics/add" method="POST" enctype="multipart/form-data" class="container-block__form text-left">
                        @csrf
                        <div class="form-group">
                            <label for="">Title</label>
                            <input type="text" class="form-control" name="title" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            <textarea name="description" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Image</label>
                            <input type="file" name="filename" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Capacity</label>
                            <input type="number" class="form-control" name="capacity" required />
                        </div>
                        <div class="form-group">
                            <label for="">Speaker</label>
                            <input type="text" class="form-control" name="speaker" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Session</label>
                            <input type="text" class="form-control" name="session" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Venue</label>
                            <input type="text" class="form-control" name="venue" required/>
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
