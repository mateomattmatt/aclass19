@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <nav aria-label="breadcrumb bg-white">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Topics</li>
                </ol>
            </nav>
            <div class="card">
                <h1>Topics</h1>
                <div class="d-flex justify-content-end">
                    <a href="{{url('/topics/add')}}" class="btn btn-primary mb-2">Add Topic</a>
                </div>
                <table class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Capacity</th>
                            <th>Registered</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@section('javascript')
<script>
    var table;
    $(document).ready(function () {
        table = $('.table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": location.href,
            "columns": [
                { "data": "id" },
                { "data": "title" },
                { "data": "capacity" },
                { "data": "registered" },
            ],
            "columnDefs": [
                {
                    "searchable": false,
                    "orderable": false,
                    "render": function ( data, type, row ) {
                        return row.users.length;
                    },
                    "targets": [3]
                },
                {
                    "searchable": false,
                    "orderable": false,
                    "render": function ( data, type, row ) {
                        return `
                            <a href="topics/${row.id}" class="mr-2"><i class="text-primary fa fa-eye"></i></a>
                            <a href="topics/${row.id}/edit" class="mr-2"><i class="text-success fa fa-edit"></i></a>
                            <a class="delete-modal" href="javascript:void(0)" data-id="${row.id}"><i class="text-danger fa fa-trash"></i></a>
                        `;
                    },
                    "targets": [4]
                },
            ],
        });
    });
    $(document).on('click', '.delete-modal', function () {
        var _id = $(this).data('id');
        swal({
            title: 'Delete this topic',
            text: 'This will remove the users from the topic? Are you sure you want to delete this?',
            dangerMode: true,
            buttons: true
        }).then(function (result) {
            if (result === true) {
                $.ajax({
                    url: '/topics/' + _id + '/delete',
                    type: 'delete'
                }).done(function (result) {
                    swal('Success', 'Successfully deleted', 'success');
                    table.ajax.url(location.href).load();
                }).fail(function (error) {
                    console.error(error);
                    swal('Error', 'Something happened', 'error');
                });
            }
        }).catch(function (error) {
            swal('Error', error, 'error');
        });
    })
</script>
@endsection
