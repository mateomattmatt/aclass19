@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <nav aria-label="breadcrumb bg-white">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="/topics">Topics</a></li>
                    <li class="breadcrumb-item"><a href="/topics/{{$topic->id}}">{{$topic->title}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Topic</li>
                </ol>
            </nav>
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="card">
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-4">
                            <h3 class="mb-0">{{$topic->title}}</h3>
                            <p>{{$topic->capacity - count($topic->users)}} slots left</p>
                        </div>
                    </div>
                </div>
                <form action="/topics/users" method="POST">
                    <div class="d-flex justify-content-between mb-3">
                        <h5>Users with no topics</h5>
                        <button type="submit" class="btn btn-primary">Add selected student</button>
                    </div>
                    <input type="hidden" name="topic" value="{{$topic->id}}">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Name</th>
                                <th>Student ID</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                @if (count($user->topics) == 0 && $user->hasRole('member'))
                                <tr>
                                    <td><input type="checkbox" name="user[]" value="{{$user->id}}"/></td>
                                    <td>
                                        <p>{{$user->first_name}} {{$user->last_name}}</p>
                                    </td>
                                    <td>
                                    <p>{{$user->student_id}}</p>
                                    </td>
                                    <td>
                                    <p>{{$user->email}}</p>
                                    </td>
                                </tr>
                                @endif 
                            @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </section>
@endsection
