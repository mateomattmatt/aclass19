@extends('layouts.app')

@section('content')
<section class="-bg-1">
    <div class="container py-4 ">
    <nav aria-label="breadcrumb bg-white">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$topic->title}}</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-12 col-md-8">
            @if ($topic->image)
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="card mb-4 p-2">
                            <img src="{{asset('aclass19/public/images/original/'.$topic->image->filename)}}" alt="" class="img-fluid" />
                        </div>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="card mb-4">
                            <h3>{{$topic->title}}</h3>
                        </div>
                        <div class="card mb-4">
                            <p>{{$topic->description}}</p>
                        </div>
                    </div>
                </div>
            @else
                <div class="card mb-4">
                    <h3>{{$topic->title}}</h3>
                </div>
                <div class="card mb-4">
                    <p>{{$topic->description}}</p>
                </div>
            @endif
        </div>
        <div class="col-12 col-md-4">
            <div class="card mb-4">
                <p><i class="fas fa-map-marker"></i> {{$topic->venue}}</p>
                <p><i class="fas fa-clock"></i> {{$topic->session}}</p>
                <p><i class="fas fa-user"></i> {{$topic->speaker}}</p>
                <p><i class="fas fa-bookmark"></i> {{$topic->capacity - count($topic->users)}} slots left</p>
                @if ($topic->capacity - count($topic->users) <= 0)
                    <div class="btn btn--main -secondary">Already full</div>
                @elseif($isTopic == 1)
                    <div class="btn btn--main -danger unjoin--class">Leave Class</div>
                @elseif($isJoin == 1)
                    <div class="btn btn--main -secondary">You already joined a class</div>
                @else
                    <div class="btn btn--main join--class">Join Class</div>
                @endif
            </div>
        </div>
    </div>
</div>
</section>
@endsection

@section('javascript')
    <script>
        $(document).on('click', '.join--class', function () {
            swal({
                title: 'Join Class',
                text: 'Are you sure about this?',
                icon: 'info',
                buttons: true
            }).then(function (result) {
                if (!!result) {
                    $.ajax({
                        url: '/topic/join',
                        data: { id: window.location.pathname.replace('/topic/', '')},
                        type: 'POST'
                    }).done(function (result) {
                        swal(result.title, result.message, result.status);
                        if (result.status === 'success') {
                            window.location.reload();
                        }
                    }).fail(function (error) {
                        swal('Error', 'Something happened', 'error');
                    })
                }
            }).catch(function (error) {
                swal('Error', JSON.stringify(error), 'error');
            });
        });

        $(document).on('click', '.unjoin--class', function () {
            swal({
                title: 'Drop Class',
                text: 'Are you sure about this?',
                icon: 'warning',
                buttons: true
            }).then(function (result) {
                if (!!result) {
                    $.ajax({
                        url: '/topic/unjoin',
                        data: { id: window.location.pathname.replace('/topic/', '')},
                        type: 'POST'
                    }).done(function (result) {
                        swal(result.title, result.message, result.status);
                        if (result.status === 'success') {
                            window.location.reload();
                        }
                    }).fail(function (error) {
                        swal('Error', 'Something happened', 'error');
                    })
                }
            }).catch(function (error) {
                swal('Error', JSON.stringify(error), 'error');
            });
        });
    </script>
@endsection
