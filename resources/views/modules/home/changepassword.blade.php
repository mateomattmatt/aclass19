@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <nav aria-label="breadcrumb bg-white">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="/profile">{{$user->first_name}} {{$user->last_name}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Change Password</li>
                </ol>
            </nav>
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="card">
                <h3>{{$user->first_name}} {{$user->last_name}}</h3>
                <br/>
                <form action="/user/{{$user->id}}/changepassword" method="POST" class="container-block__form">
                    @csrf
                    <div class="form-group">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="New Password">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                    </div>
                    <div class="form-group text-right">
                        <button class="btn btn--main" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

