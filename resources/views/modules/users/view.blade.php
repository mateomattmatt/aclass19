@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <nav aria-label="breadcrumb bg-white">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="/users">Users</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$user->first_name}} {{$user->last_name}}</li>
                </ol>
            </nav>
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="card">
                <h3>{{$user->first_name}} {{$user->last_name}}</h3>
                <a href="/users/{{$user->id}}/changepassword">Change Password</a>
                <br>
                <div class="container-block__content">
                    <form action="/users/{{$user->id}}" method="POST" class="container-block__form">
                        @csrf
                        <div class="row">
                            <div class="form-group col-12 col-md-6">
                                <label for="">First name</label>
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" required autofocus placeholder="First Name" value="{{$user->first_name}}">
                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <label for="">Last name</label>
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" required autofocus placeholder="Last Name" value="{{$user->last_name}}">
                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Student ID</label>
                            <input id="student_id" type="text" class="form-control{{ $errors->has('student_id') ? ' is-invalid' : '' }}" name="student_id" required autofocus placeholder="Student ID" value="{{$user->student_id}}">
                            @if ($errors->has('student_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('student_id') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" required placeholder="Email address" value="{{$user->email}}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Roles</label>
                            <select name="role" class="form-control">
                                <option disabled>-- Choose user role --</option>
                                @foreach (\App\Role::orderBy('id', 'DESC')->get() as $role)
                                    @if($role->name != 'owner' || Auth::user()->hasRole('owner'))
                                        @if($user->roles[0]->name == $role->name)
                                            <option value="{{$role->name}}" selected>{{$role->name}}</option>
                                        @else
                                            <option value="{{$role->name}}">{{$role->name}}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn--main" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection

