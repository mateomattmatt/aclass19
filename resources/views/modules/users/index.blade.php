@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <nav aria-label="breadcrumb bg-white">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Users</li>
                </ol>
            </nav>
            <div class="card">
                <h1>Users</h1>
                <div class="d-flex justify-content-end">
                    <a href="{{url('/users/add')}}" class="btn btn-primary mb-2">Add User</a>
                </div>
                <table id="user-table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Stud ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@section('javascript')
<script>
    var table;
    $(document).on('click', '.delete-user', function () {
        var id = $(this).data('id');
        swal({
            title: 'Delete User',
            text: 'Are you sure about this?',
            icon: 'warning',
            buttons: true
        }).then(function (result) {
            if (!!result) {
                $.ajax({
                    url: '/users/',
                    data: { id: id },
                    type: 'DELETE'
                }).done(function (result) {
                    swal(result.title, result.message, result.status);
                    if (result.status === 'success') {
                        table.ajax.reload();
                    }
                }).fail(function (error) {
                    swal('Error', 'Something happened', 'error');
                })
            }
        }).catch(function (error) {
            swal('Error', JSON.stringify(error), 'error');
        });
    });
    $(document).ready(function() {
        table = $('#user-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": location.href,
            "columns": [
                    { "data": "student_id" },
                    { "data": "first_name" },
                    { "data": "last_name" },
                    { "data": "email" },
                    { "data": "role" },
                    { "data": "action" },
                ],
            "columnDefs": [
                    {
                        "orderable": false,
                        "render": function ( data, type, row ) {
                            return '<a>' + row.roles[0].name + '</a>';
                        },
                        "targets": [4]
                    },
                    {
                        "searchable": false,
                        "orderable": false,
                        "render": function ( data, type, row ) {
                            if (row.roles[0].name !== 'owner') {
                                return `
                                    <a href="users/${row.id}" class="mr-2"><i class="text-primary fa fa-eye"></i></a>
                                    <span data-id="${row.id}" class="delete-user mr-2" style="cursor: pointer;"><i class="fa fa-trash text-danger "></i></span>
                                `;
                            } else {
                                return '';
                            }
                        },
                        "targets": [5]
                    },
                ],
        });

    } );
</script>
@endsection
