@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <nav aria-label="breadcrumb bg-white">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Logs</li>
                </ol>
            </nav>
            <div class="card">
                <h1>Logs</h1>
                <table class="table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>user</th>
                            <th>message</th>
                            <th>ip</th>
                            <th>user_agent</th>
                            <th>city</th>
                            <th>region</th>
                            <th>country</th>
                            <th>loc</th>
                            <th>postal</th>
                            <th>org</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@section('javascript')
<script>
    $(document).ready(function () {
        $('.table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": location.href,
            "columns": [
                { "data": "id" },
                { "data": "" },
                { "data": "message" },
                { "data": "ip_address" },
                { "data": "user_agent" },
                { "data": "city" },
                { "data": "region" },
                { "data": "country" },
                { "data": "loc" },
                { "data": "postal" },
                { "data": "org" },
            ],
            "columnDefs": [
                {
                    "searchable": true,
                    "orderable": true,
                    "render": function ( data, type, row ) {
                        return row.user.first_name + ' ' + row.user.last_name;
                    },
                    "targets": [1]
                },
            ],
        });
    });
</script>
@endsection
