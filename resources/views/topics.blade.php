@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="container-block">
            <div class="container-block__header">
                <h1>Edit Topic</h1>
                <hr/>
            </div>
            <form action="/topics/add" method="POST" class="text-left">
                @csrf
                <div class="form-group">
                    <label for="">Title</label>
                    <input type="text" class="form-control" name="title" value={{$topic->title}} required/>
                </div>
                <div class="form-group">
                    <label for="">Description</label>
                    <textarea name="description" class="form-control" required>{{$topic->description}}</textarea>
                </div>
                <div class="form-group">
                    <label for="">Capacity</label>
                    <input type="number" class="form-control" name="capacity" value={{$topic->capacity}} required />
                </div>
                <button class="btn btn-primary" type="submit">Submit</button>
            </form>
        </div>
    </div>
@endsection
